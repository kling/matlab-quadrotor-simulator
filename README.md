# README #

A matlab quadrotor simulator used during my master's research. The main quadrotor dynamics simulation was originally created by Professor Steven L. Waslander at the University of Waterloo.

This project is offered as is for educational purposes. All I ask is that we are given some form of acknowledgement if this code is used in whole or any significant part.

The entry point to the simulation is in SimpleQuadrotor.m

The related thesis can be found here: https://uwspace.uwaterloo.ca/handle/10012/8803