%%  Motor model testing

% Load parameter file
motor_parameters;

%% Motor curves
V = 0:0.1:V_max;
omega = 1/(2*k_tau*k_t) * (-k_q*k_e/Ra + sqrt((k_q*k_e/Ra)^2+4*(k_tau*k_t)*(k_q.*V/Ra)));
%omega = 0:1:1000;
%V = Ra/k_q*k_tau*k_t.*omega2.^2 + k_e*omega2;
I = V/Ra-k_e/Ra*omega;
Thrust = k_t*omega.^2;
Torque_a = k_tau*Thrust;
Torque_m = k_q*I;
figure(1); clf;
subplot(3,1,1); hold on
plot(V, omega*60/2/pi);
xlabel('Voltages - VDC');
ylabel('Rotor speed - rpm')
title('Steady state mapping from voltages to Speed, Thrust and Torque')
subplot(3,1,2); hold on;
plot(V, Thrust/g);
xlabel('Voltages - VDC');
ylabel('Thrust - kg  (= N/g)') 
subplot(3,1,3); hold on;
plot(V, Torque_a,'b'); 
plot(V, Torque_m,'r--'); 
xlabel('Voltages - VDC');
ylabel('Torque - Nm') 
legend('Aero drag torque', 'Motor torque')

figure(3); clf; hold on;
plot(Thrust, V.*I);
xlabel('Thrust - N');
ylabel('Power consumed - W')
title('Thrust to Power for 0-11 VDC')


%% Motor Simulation

% Time vector setup
T = 1;
dt = 0.0001;
Tvec = 0:dt:T;
nT = length(Tvec);

% Reference signal setup
xd = omega_nom*ones(1,nT); % (xd, yd, zd, 0, 0, 0, 0, 0, yawd, 0, 0, 0);
% Siunsoidal inputs
%xd(1,:) = 450+100*sin(10*Tvec); % Desired rotor speed
% Step inputs
xd(1,round(nT/4):round(nT/2)) = 450;
xd(1,round(nT/2):round(3*nT/4)) = 420;

% Disturbance setup
sigma = 0; % speed disturbance standard deviation

% State and input storage setup
x = zeros(1,nT);
x(1) = omega_nom;
u = zeros(1,nT); % Thrust commands
tau = zeros (1,nT); % rotor torques
Vi = zeros(1,nT); % motor voltages
Vi_ff = zeros(1,nT); % motor voltages
%Vi_fb = zeros(1,nT); % motor voltages

% Controller setup
ecur = zeros(1,nT);
eint = zeros(1,nT);

%% Main simulation loop
for t=2:nT
    % Motor speed control
    ecur(t) = xd(t-1) - x(t-1); % Rotor speed errors
    eint(t) = eint(t-1) + dt*ecur(t);
    Vi_ff(t) = Ra/k_q*k_tau*k_t*xd(t-1)^2 + k_e*xd(t-1);
    Vi_fb(t) = Kms*[ecur(t); eint(t)];
    Vi(t) = Vi_ff(t) +Vi_fb(t);
    Vi(t) = min(V_max,max(Vi(t),0)); % Bound voltage command
    
    % Rotor torque model
    tau(t) = k_tau * k_t * x(t-1)^2; % Rotor aero torques at current speed
    % Rotor dynamics
    d = sigma*randn(1,1);
    x(t) = x(t-1) + 1/Jr * dt*(- tau(t) - (k_q*k_e/Ra)*x(t-1) + (k_q/Ra)*Vi(t)) + d; % Rotor speeds

end
%% Plot results

figure(2); clf;
subplot(4,1,1); hold on;
plot(Tvec,x);
plot(Tvec,xd,':');
title('Rotor speed');

subplot(4,1,2); hold on;
plot(Tvec, Vi, 'b');
plot(Tvec, Vi_ff, 'r');
plot(Tvec, Vi_fb, 'g');
title('Voltages');
legend('Total','Feedforward', 'Feedback')

subplot(4,1,3); hold on;
plot(Tvec, tau,'b');
plot(Tvec, (k_q/Ra)*Vi,'r');
plot(Tvec, (k_q*k_e/Ra)*x,'g');
title('Torques');
legend('Aero torque','Ideal Motor torque', 'Back EMF loss')

subplot(4,1,4); hold on;
plot(Tvec, k_t * x.^2,'b');
title('Thrust');




