%% Simulation of a quadrotor v2.0
clear; close all;
addpath('qr_sim_functions')


%% Set up simulation

% Time vector setup
T = 10;
dt = 0.001; % set dt first

Tvec = 0:dt:T;
nT = length(Tvec);

% Initialize simulation
qr_sim = initializeqrsim(dt);

% Get qr position
getqractualposition(qr_sim)


%% Reference signal setup
xd = zeros(6,nT); % (xd, yd, zd, 0, 0, 0, 0, 0, yawd, 0, 0, 0);
scenario  = 2;  % Select simulation scenario, last case is for playing with, 
% leave others as test cases.

switch scenario
    case 1,
        % Scenario 1: Oscillations in X,Y,Z and yaw
        qr_sim.pos = [0; 0; 2.5-2.0*sin(pi/2)]; % Initial position
        xd(1,:) = 10*sin(0.01*Tvec); % X position reference
        xd(2,:) = -8*sin(0.09*Tvec); % Y position reference
        xd(3,:) = 2.5-2.0*sin(0.2*Tvec + pi/2); % Z position reference
        xd(4:6,1:nT-1) = (diff(xd(1:3,:)')'/dt); % Forward difference for vel ref
        xd(9,:) = deg2rad(15)*sin(1*Tvec); % Yaw reference
        disp('Scenario 1 selected');
    case 2,
        % Scenario 2: Oscillations in X, only 
        qr_sim.pos = [0; 0; 2.5]; % Initial position
        xd(2,:) = 2*sin(0.5*Tvec); % X position reference
        xd(1,:) = 0*Tvec;  % Y position reference
        xd(3,:) = 2.5 + 0*Tvec; % Z position reference
        xd(4:6,1:nT-1) = (diff(xd(1:3,:)')'/dt); % Forward difference for vel ref
        xd(9,:) = 0*Tvec; % Yaw reference
        disp('Scenario 2 selected');
    otherwise,
        % Arbitrary scenario
        qr_sim.pos = [0; 0; 2.5-2.0*sin(pi/2)]; % Initial position
        xd(1,:) = 10*sin(0.01*Tvec); % X position reference
        xd(2,:) = -8*sin(0.09*Tvec); % Y position reference
        xd(3,:) = 2.5-2.0*sin(0.2*Tvec + pi/2); % Z position reference
        xd(4:6,1:nT-1) = (diff(xd(1:3,:)')'/dt); % Forward difference for vel ref
        xd(9,:) = deg2rad(15)*sin(1*Tvec); % Yaw reference
        disp('Scenario X, (whatever you set it to) selected');

end

% Define remaining state variables for simulation
pos_out = zeros(3, nT);
vel_out = zeros(3, nT);
ang_out = zeros(3, nT);
pqr_out = zeros(3, nT);
rot_omega_out = zeros(4, nT);
wind_out = zeros(3, nT);
accel_clean_out = zeros(3, nT);
ctrl_sigs_out = zeros(4, nT); % [uphi, utheta, upsi, uz]
forces_out = zeros(4,nT);

% Define sensor variables
gps_out = zeros(3, nT);
gps_vel_out = zeros(3, nT);
mag_out = zeros(1, nT);
accel_out = zeros(3, nT);
gyro_out = zeros(3, nT);
sonar_out = zeros(1, nT);
barom_out = zeros(1, nT);


tic
%% Main simulation loop
for t=1:nT
    qr_sim = qrsimupdate(qr_sim, xd(1:3, t), xd(4:6, t), xd(9,t));
    
    pos_out(:,t) = qr_sim.pos;
    vel_out(:,t) = qr_sim.vel;
    ang_out(:,t) = qr_sim.rpy;
    pqr_out(:,t) = qr_sim.pqr;
    wind_out(:,t) = qr_sim.last_wind;
    rot_omega_out(:,t) = qr_sim.rot_omega;
    accel_clean_out(:,t) = qr_sim.accel_clean;
    ctrl_sigs_out(:,t) = [qr_sim.ctrl_sigs(4:6); qr_sim.ctrl_sigs(3)];
    
    gps_out(:,t) = qr_sim.gps;
    gps_vel_out(:,t) = qr_sim.gps_vel;
    sonar_out(:,t) = qr_sim.sonar;
    barom_out(:,t) = qr_sim.barom;
    gyro_out(:,t) = qr_sim.gyro;
    accel_out(:,t) = qr_sim.accel;
    mag_out(:,t) = qr_sim.mag;
    forces_out(:,t) = qr_sim.generated_forces;
end

toc

%%
% animatesimulation

%%
generateqrplots
