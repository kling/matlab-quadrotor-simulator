% Effect of reduction of Inertia on attitude control

% Assume first order lag on control torque to speed command (dropping
% scaling by length of arm, sum of both forces, scaling from thrust to
% torque)
% dot(torque_phi) = - 1/tau torque_phi + u

% Laplaced
% Torque_phi = U/(tau s + 1)

% From Newton, ignoring rotating frame, drag, anything else
% I_phi ddot(phi) = torque_phi

% Laplaced
% I_phi s^2 Phi = Torque_phi

% Combined
% I_phi s^2 Phi = U/(tau s + 1)
% Phi = U/I_phi(tau s^3 + s^2)

% Control PID,DD
%U = (Kdd s^3 + Kd s^2 + Kp s + Ki) / s

I_phi = 5;
tau = 0.1;
Kdd = 1;
Kd = 10;
Kp = 10;
Ki = 1;

sys = tf([1], I_phi*[tau 1 0 0]);

cntrl = tf([ Kdd Kd Kp Ki], [1 0]);

sisotool(sys,cntrl);




