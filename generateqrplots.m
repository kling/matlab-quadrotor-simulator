%% Position tracking plot
figure; hold on; grid on
plot(Tvec, xd(1,:), '--b')
plot(Tvec, xd(2,:), '--c')
plot(Tvec, xd(3,:), '--r')
plot(Tvec, pos_out(1,:), 'b')
plot(Tvec, pos_out(2,:), 'c')
plot(Tvec, pos_out(3,:), 'r')
title('XYZ Position, Desired vs Actual');
legend('X_d','Y_d','Z_d','X','Y','Z')
axis equal

%% Position tracking plot
figure; grid on; hold on
plot3(xd(1,:), xd(2,:), xd(3,:),'b--');
plot3(pos_out(1,:), pos_out(2,:), pos_out(3,:),'r');
plot3(gps_out(1,:), gps_out(2,:), gps_out(3,:),'g:');
xlabel('x');
ylabel('y');
zlabel('z');
title('3D Position, Desired vs GPS');
legend('Desired', 'Actual', 'GPS')

%% GPS position measurement plot
figure; grid on; hold on
title('3D Position, Actual vs GPS');
plot3(gps_out(1,:), gps_out(2,:), gps_out(3,:), '.b');
plot3(xd(1,:), xd(2,:), xd(3,:), 'r');
legend('GPS', 'Truth');
xlabel('x');
ylabel('y');
zlabel('z');



%% Euler angles plot
figure; hold on; grid on
title('Euler Angles, Measured');
plot(Tvec, rad2deg(ang_out(1,:)))
plot(Tvec, rad2deg(ang_out(2,:)), 'c')
plot(Tvec, rad2deg(ang_out(3,:)), 'g')
xlabel('Time, s');
ylabel('Angle, deg.');
legend('roll', 'pitch', 'yaw');
% 
% figure; hold on; grid on
% title('Wind');
% plot(Tvec, wind_out(1,:))
% plot(Tvec, wind_out(2,:), 'c')
% plot(Tvec, wind_out(3,:), 'g')
% legend('x', 'y', 'z');

%%  Gyro plot
figure; hold on; grid on
title('Gyroscope Measurements');
plot(Tvec, rad2deg(gyro_out(1,:)))
plot(Tvec, rad2deg(gyro_out(2,:)), 'c')
plot(Tvec, rad2deg(gyro_out(3,:)), 'g')
plot(Tvec, rad2deg(pqr_out(1,:)), '--b')
plot(Tvec, rad2deg(pqr_out(2,:)), '--c')
plot(Tvec, rad2deg(pqr_out(3,:)), '--g')
xlabel('Time, s');
ylabel('Angular rate, deg. / s');
legend('x', 'y', 'z', 'true x', 'true y', 'true z');

%% Integrated gyro plot
int_gyro = cumsum(gyro_out);
int_clean_gyro = cumsum(pqr_out);
figure; hold on; grid on
title('Integrated gyro based angles');
plot(Tvec, rad2deg(int_gyro(1,:)))
plot(Tvec, rad2deg(int_gyro(2,:)), 'c')
plot(Tvec, rad2deg(int_gyro(3,:)), 'g')
plot(Tvec, rad2deg(int_clean_gyro(1,:)), '--b')
plot(Tvec, rad2deg(int_clean_gyro(2,:)), '--c')
plot(Tvec, rad2deg(int_clean_gyro(3,:)), '--g')
legend('x', 'y', 'z');

%% Altitude plot
figure; hold on; grid on;
title('Altitude, Actual vs Measured (Sonar and Barometer)');
plot(Tvec, pos_out(3,:), 'r');
plot(Tvec, sonar_out(1,:),'b');
plot(Tvec, barom_out(1,:), 'm');
legend('Actual', 'Sonar', 'Barometric')

%%
% figure; hold on; grid on;
% title('Yaw');
% plot(Tvec, rad2deg(ang_out(3,:)), '--r');
% plot(Tvec, rad2deg(mag_out(1,:)));
%%
figure; hold on; grid on;
title('Accelerometer Measurements');
plot(Tvec, accel_clean_out(1,:), '--b');
plot(Tvec, accel_clean_out(2,:), '--r');
plot(Tvec, accel_clean_out(3,:), '--g');
plot(Tvec, accel_out(1,:), 'b');
plot(Tvec, accel_out(2,:), 'r');
plot(Tvec, accel_out(3,:), 'g');
legend('x','y','z','x - measured','y - measured','z - measured')
