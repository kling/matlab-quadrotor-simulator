%% Rotor and motor coefficients 
% based on coax sys id from Siegwart and nominal quadrotor design
% coax is 7" rotors, but similar motors (only 2)

% Define environment parameters
environment_parameters;

% Rotor
% r = 6*0.0254; % m rotor radius (half length) (Siegwart 0.0875)
Jr = 1.93e-5; % kg m^2 rotor inertia calculated from 1/12 m l^2, 10 gm, 6") (Siegwart 1.9e-5)
k_t = 1.95e-5; % Ns^2/rad^2 coefficient of thrust (Siegwart 0.0115 pi rho r^4 = 2.6e-6 )
k_tau = 1.4e-2; % Nm s^2/rad^2 coefficient of torque drag (Siegwart kt*k_tau = 0.0018 pi rho r^5 = 3.553e-8 )


% Motor
k_q = 0.035; % Nm / A coefficient of motor torque ( Siegwart 0.0035 )
k_e = 0.0025; % 1/ V s motor back emf constant (Siegwart 0.0045 )
Ra = 1.3; % ohms armature resistance (online for a standard BLDC, and Siegwart)
V_max = 11 ;% VDC

% Nominal operating point - used to verify coefficients only
omega_nom = 420; % rad/ s  nominal rotor speed 4000 rpm = 420 rad/s
V_nom = Ra/k_q*k_t*k_tau*omega_nom^2 + k_e*omega_nom; % VDC Nominal voltage 
I_nom = V_nom/Ra-k_e/Ra*omega_nom;
T_nom = k_t*omega_nom^2;
Tau_nom = k_tau*T_nom;

% Motor speed control
Kpms = 0.1;
Kims = 0.1;
Kms = [Kpms,Kims];
