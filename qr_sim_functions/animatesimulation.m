%% Cool simulator
tic
qm_matfilename = 'quad_starmac.mat';    %filename for the mat file for the quadrotor model
load(qm_matfilename);

axis_x_min = min(pos_out(1,:))-1;
axis_x_max = max(pos_out(1,:))+1;
axis_y_min = min(pos_out(2,:))-1;
axis_y_max = max(pos_out(2,:))+1;
axis_z_min = min(pos_out(3,:))-1;
axis_z_max = max(pos_out(3,:))+1;

figure(5); clf;
tic
t_real = toc;
for t = 1:100:nT
    clf; hold on;
    draw_quadrotor( qm_fnum, qm_xyz, ang_out(1,t), ang_out(2,t), ...
        ang_out(3,t), pos_out(1,t), pos_out(2,t), pos_out(3,t),...
        [0.6 0.1 0.1]);
    %adjust rendering style
    lighting phong;
    camlight right;
    %adjust graph viewing aspects
    grid on;
    axis square;
    axis([axis_x_min, axis_x_max, axis_y_min, axis_y_max, axis_z_min, axis_z_max]);
    view(30,10);
    xlabel('X'); ylabel('Y'); zlabel('Z');
    drawnow;
end

toc