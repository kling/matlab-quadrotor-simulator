function [ position ] = getqractualposition( qr )
%GETQRACTUALPOSITION Returns actual quadrotor position.
%   Extracts and returns the [x,y,z] states out of the quadrotor struct.

position = qr.pos;

end

