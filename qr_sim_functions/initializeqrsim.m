function qr_sim = initializeqrsim(dt)
% Make sure that setup will not be destroying any variables
% unintentionally. These variable names should not be used.

%clear qr_sim
%assert(~exist('qr_sim', 'var'), 'Initialize QR Sim: Variable qr_sim already exists.');

freq__ = 1 / dt;

qr_sim = struct([]);

qr_sim(1).dt = dt;

%% Options
qr_sim.ENABLE_NOISE = 0;
qr_sim.ENABLE_SONAR_NOISE = 0;
qr_sim.ENABLE_DISTURBANCES = 0;
qr_sim.ENABLE_WIND = 0;

%% Environment parameters
qr_sim.RHO = 1.225; % air density
qr_sim.GRAV = 9.81;  % gravity

%% Rotor and motor parameters
% Rotor
% r = 6*0.0254; % m rotor radius (half length) (Siegwart 0.0875)
qr_sim.Jr = 1.93e-5; % kg m^2 rotor inertia calculated from 1/12 m l^2, 10 gm, 6") (Siegwart 1.9e-5)
qr_sim.k_t = 1.95e-5; % Ns^2/rad^2 coefficient of thrust (Siegwart 0.0115 pi rho r^4 = 2.6e-6 )
qr_sim.k_tau = 1.4e-2; % Nm s^2/rad^2 coefficient of torque drag (Siegwart kt*k_tau = 0.0018 pi rho r^5 = 3.553e-8 )

% Motor
qr_sim.k_q = 0.035; % Nm / A coefficient of motor torque ( Siegwart 0.0035 )
qr_sim.k_e = 0.0025; % 1/ V s motor back emf constant (Siegwart 0.0045 )
qr_sim.Ra = 1.3; % ohms armature resistance (online for a standard BLDC, and Siegwart)
qr_sim.V_max = 11 ;% VDC

% Nominal operating point - used to verify coefficients only
qr_sim.omega_nom = 420; % rad/ s  nominal rotor speed 4000 rpm = 420 rad/s
qr_sim.V_nom = qr_sim.Ra/qr_sim.k_q*qr_sim.k_t*qr_sim.k_tau*qr_sim.omega_nom^2 + qr_sim.k_e*qr_sim.omega_nom; % VDC Nominal voltage 
qr_sim.I_nom = qr_sim.V_nom / qr_sim.Ra - qr_sim.k_e / qr_sim.Ra*qr_sim.omega_nom;
qr_sim.T_nom = qr_sim.k_t*qr_sim.omega_nom^2;
qr_sim.Tau_nom = qr_sim.k_tau*qr_sim.T_nom;


%% Quadrotor model parameters
qr_sim.N = 16; % number of state variables
qr_sim.M = 4; % number of inputs

% Vehicle Mass
m_c__ = 0.400; % kg mass of quadrotor core
m_m__ = 0.15; % kg mass of quadrotor motors
m__ = m_c__ + 4*m_m__; % mass of quadrotor
qr_sim.mass = m__;

qr_sim.l_c = 0.1; % m center length
qr_sim.l_m = 0.8; % m  arm length

% Inertia calculated from four motors and a cube center.
Ix__ = 1/12*m_c__*qr_sim.l_c^2 + 2*(1/2*m_m__*qr_sim.l_m^2); 
Iy__ = Ix__;
Iz__ = 1/12*m_c__*(qr_sim.l_c^2+qr_sim.l_c^2) + 4*(1/2*m_m__*qr_sim.l_m^2);
qr_sim.I = diag([Ix__, Iy__, Iz__]);

% Frontal area assumes l_c cube at center, 1 cm arms and 4cm X1cm radius
% motor cylinders.  Ignores rotors, as their drag is accounted for
% elsewhere
qr_sim.Axy = qr_sim.l_c^2 + 2*0.01*0.70 + 2*0.04*0.02; % x-y frontal area
qr_sim.Az = qr_sim.l_c^2 + 4*pi*0.01^2 + 4*0.01*0.7; % z frontal area
qr_sim.c_d = 1.0; % translational coefficient of drag
qr_sim.c_m = 1.0; % rotational coefficient of drag

qr_sim.Mcr = [   0  qr_sim.l_m     0   -qr_sim.l_m;
                -qr_sim.l_m     0  qr_sim.l_m     0; % Conversion from rotor pwm to control torques/thrust
                -qr_sim.c_m  qr_sim.c_m   -qr_sim.c_m  qr_sim.c_m;
                1    1    1    1];

qr_sim.Mrc = [0  -1/(2*qr_sim.l_m)   -1/(4*qr_sim.c_m) 1/4; % Conversion from control torques/thrust to rotor pwm
            1/(2*qr_sim.l_m) 0 1/(4*qr_sim.c_m) 1/4;
            0  1/(2*qr_sim.l_m)  -1/(4*qr_sim.c_m) 1/4;
            -1/(2*qr_sim.l_m) 0 1/(4*qr_sim.c_m) 1/4];

%  Drag coefficients
qr_sim.Cdxy   = 1/2*qr_sim.Axy*qr_sim.RHO*qr_sim.c_d; 
qr_sim.Cdz    = 1/2*qr_sim.Az*qr_sim.RHO*qr_sim.c_d;
qr_sim.Cmxy   = 1/2*qr_sim.Az*qr_sim.RHO*qr_sim.c_m;  
qr_sim.Cmz    = 1/2*qr_sim.Axy*qr_sim.RHO*qr_sim.c_m; 

% Ground effect coefficients
qr_sim.Kge = 0.5; % Ground effect percent thrust increase
qr_sim.hge = 1.0; % Maximum height for ground effect model to have effect



%% Quadrotor controller parameters
% Position control
Kpxy__ = 0.5;
Kdxy__ = 0.5;
Kixy__ = 0.32;
qr_sim.Kxy = [Kpxy__,Kdxy__,Kixy__];

% Altitude control
Kpz__ = 10;
Kdz__ = 5;
Kiz__ = 0.0;
Kgz__ = qr_sim.mass*qr_sim.GRAV; % gravity offset, base desired force to overcome gravity
qr_sim.Kz = [Kpz__,Kdz__,Kiz__,Kgz__];

% Roll/Pitch control
Kpatt__ = 10;
Kdatt__ = 2;
Kddatt__ = 0.0;
Kiatt__ = .1;
qr_sim.Katt = [Kpatt__,Kdatt__,Kddatt__,Kiatt__];
qr_sim.MAX_RP = 30*pi/180;
qr_sim.MAX_RP_RATE = 10;

% Yaw control
Kpyaw__ = 5;
Kdyaw__ = 2;
Kddyaw__ = 0;
Kiyaw__ = 2;
qr_sim.Kyaw = [Kpyaw__,Kdyaw__,Kddyaw__,Kiyaw__];

% Motor speed control
Kpms__ = 0.1;
Kims__ = 0.1;
qr_sim.Kms = [Kpms__,Kims__];

qr_sim.Td_max = 15; % (N) Max thrust request per motor

%% Controller states (integrators and intermediate signals)
qr_sim.err_pos_int = [0;0;0];
qr_sim.err_att_int = [0;0;0];
qr_sim.err_mot_int = [0;0;0;0];

% ctrl sigs: [ux, uy, uz, uphi, utheta, upsi]
qr_sim.ctrl_sigs = [0;0;0;0;0;0];

%% Controller update rates
qr_sim.POS_CTRL_TICK_RATE = freq__ / 20;
qr_sim.POS_CTRL_DT = 1 / 5;
qr_sim.pos_ctrl_ticks = 0;
qr_sim.ATT_CTRL_TICK_RATE = freq__ / 100;
qr_sim.ATT_CTRL_DT = 1 / 100;
qr_sim.att_ctrl_ticks = 0;


%% Set up states
qr_sim.pos = [0;0;0]; % positions, m
qr_sim.vel = [0;0;0]; % linear velocities, m/s
qr_sim.rpy = [0;0;0]; % euler angles, rad
qr_sim.pqr = [0;0;0]; % body angular rates, rad/s
qr_sim.rot_omega = qr_sim.omega_nom.*ones(4,1); % motor angular speeds

qr_sim.des_pos = [0;0;0];
qr_sim.des_vel = [0;0;0];
qr_sim.des_rpy = [0;0;0];
qr_sim.des_rpy_rate = [0;0;0];
qr_sim.des_rot_omega = [0;0;0;0];
qr_sim.mot_v = [0;0;0;0];

%% Sensor update rates
qr_sim.GPS_FREQ = 5; % hz
qr_sim.GPS_TICK_RATE = round(max(1, freq__ / qr_sim.GPS_FREQ)); % Tick period
qr_sim.gps_ticks = qr_sim.GPS_TICK_RATE + 1;
qr_sim.BAROM_FREQ = 100;
qr_sim.BAROM_TICK_RATE = round(max(1, freq__ / qr_sim.BAROM_FREQ));
qr_sim.barom_ticks = qr_sim.BAROM_TICK_RATE + 1;
qr_sim.SONAR_FREQ = 20;
qr_sim.SONAR_TICK_RATE = round(max(1, freq__ / qr_sim.SONAR_FREQ));
qr_sim.sonar_ticks = qr_sim.SONAR_TICK_RATE + 1;
qr_sim.IMU_FREQ = min(500, freq__);
qr_sim.IMU_TICK_RATE = round(max(1, freq__ / qr_sim.IMU_FREQ));
qr_sim.imu_ticks = qr_sim.IMU_TICK_RATE + 1;

%% Bias states for sensors. Added to sensors for outputs
qr_sim.gps_bias = [0; 0; 0];
qr_sim.barom_bias = 0;
qr_sim.gyro_bias = [0; 0; 0];
qr_sim.accel_bias = [0; 0; 0];
qr_sim.mag_bias = 0;

%% Sensor covariances
if (qr_sim.ENABLE_NOISE)
    qr_sim.GPS_COV = [0.5; 0.5; 0.7] * qr_sim.GPS_TICK_RATE*dt;
    qr_sim.GPS_VEL_COV = [0.2; 0.2; 0.4] * qr_sim.GPS_TICK_RATE*dt;
    qr_sim.GPS_BIAS_COV = [1; 1; 1] * qr_sim.GPS_TICK_RATE*dt;
    qr_sim.BAROM_COV = 0.5 * qr_sim.BAROM_TICK_RATE*dt;
    qr_sim.BAROM_BIAS_COV = 1.2 * qr_sim.BAROM_TICK_RATE*dt;
    qr_sim.SONAR_COV = 0.04 * qr_sim.SONAR_TICK_RATE*dt;

    qr_sim.GPS_TAU = 90;
    qr_sim.BAROM_TAU = 1500;
    qr_sim.GYRO_TAU = 100;
    qr_sim.MAG_TAU = 5000;

    % Just multiply by dt for now for discretization, IMU runs fast enough
    qr_sim.GYRO_COV = 0.025*(1/qr_sim.IMU_FREQ)*ones(3,1); % regular noise covariance
    qr_sim.GYRO_BIAS_COV = 0.005*(1/qr_sim.IMU_FREQ)*ones(3,1);
    qr_sim.ACCEL_COV = 0.125*(1/qr_sim.IMU_FREQ)*ones(3,1);
    qr_sim.MAG_COV = deg2rad(9) * (1/qr_sim.IMU_FREQ);
    qr_sim.MAG_BIAS_COV = deg2rad(35)*(1/qr_sim.IMU_FREQ);
else
    qr_sim.GPS_COV = zeros(3,1);
    qr_sim.GPS_VEL_COV = zeros(3,1);
    qr_sim.GPS_BIAS_COV = zeros(3,1);
    qr_sim.BAROM_COV = 0;
    qr_sim.BAROM_BIAS_COV = 0;
    qr_sim.SONAR_COV = 0;

    qr_sim.GYRO_COV = zeros(3,1);
    qr_sim.GYRO_BIAS_COV = zeros(3,1);
    qr_sim.ACCEL_COV = zeros(3,1);
    qr_sim.MAG_COV = 0;
    qr_sim.MAG_BIAS_COV = 0;
end

qr_sim.GPS_TAU = 90;
qr_sim.BAROM_TAU = 1500;
qr_sim.GYRO_TAU = 100;
qr_sim.MAG_TAU = 5000;

%% Sensor measurements, get updated at their specified rates
qr_sim.gps = [0; 0; 0];
qr_sim.gps_vel = [0; 0; 0];
qr_sim.barom = 0;
qr_sim.sonar = 0;
qr_sim.gyro = [0; 0; 0];
qr_sim.accel = [0; 0; 0];
qr_sim.accel_clean = [0; 0; 0];
qr_sim.mag = 0;

qr_sim.last_wind = [0;0;0];

qr_sim.generated_forces = zeros(3,1);