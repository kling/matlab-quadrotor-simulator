function [ qr ] = qrsimupdate( qr, des_pos, des_vel, des_yaw )
%QRSIMUPDATE Summary of this function goes here
%   Detailed explanation goes here

    qr.des_pos = des_pos;
    qr.des_vel = des_vel;

    %% Run controllers
    qr.pos_ctrl_ticks = qr.pos_ctrl_ticks + 1;
    if qr.pos_ctrl_ticks > qr.POS_CTRL_TICK_RATE
        qr = updateposctrl(qr);
        qr.pos_ctrl_ticks = 0;
    end
    
    qr.des_rpy(3) = des_yaw;
    
    qr.att_ctrl_ticks = qr.att_ctrl_ticks + 1;
    if qr.att_ctrl_ticks > qr.ATT_CTRL_TICK_RATE
        qr = updateattctrl(qr);
        qr.att_ctrl_ticks = 0;
    end
    
    qr = updatemotorctrl(qr);
    
    
    %% Vehicle simulation
    % Rotation from body to inertial
    R = (rot(qr.rpy(1),1)*rot(qr.rpy(2),2)*rot(qr.rpy(3),3))'; 
    
    % Conversion of body rates to euler rates, Re(angles, b2e=1)
    R_e = re(qr.rpy,1); 
    
    % Rotor model
    tau = qr.k_t * qr.k_tau * qr.rot_omega.^2; % Rotor aero torques at current speed
    f = qr.k_t * qr.rot_omega.^2; % Rotor thrust model
    
    % Ground effect model (does not consider rotation of frame)
    f_ge = qr.Kge*(qr.hge-min(qr.hge,qr.pos(3)))^2/(qr.hge^2).*f;
    
    % Rotor forces and moments
    f_t = R*[0 0 sum(f+f_ge)]'; % Thrust forces in inertial frame
    M_t = qr.Mcr(1:3,:)*(f+f_ge); % Rotor moments in body frame

    qr.generated_forces = [M_t; sum(f)];
    
    % Gravity model
    f_g = [0 0 qr.mass*qr.GRAV]'; % inertial frame gravity
    
    % Drag force model
    wind = generate_wind(qr);
    qr.last_wind = wind;
    v_air = R'*(qr.vel - wind); % vehicle velocity in body coordinates
    f_dx = sign(v_air(1)) * qr.Cdxy*(v_air(1))^2;
    f_dy = sign(v_air(2)) * qr.Cdxy*(v_air(2))^2;
    f_dz = sign(v_air(3)) * qr.Cdz*(v_air(3))^2;
    f_d = R*[f_dx; f_dy; f_dz]; % Drag force in inertial frame

    % Drag moment model
    M_d = [qr.Cmxy*qr.pqr(1:2); qr.Cmz*qr.pqr(3)];
    
    % Disturbance model
    dist_cov = diag([0.01 0.01 0.01 0.01 0.01 0.01]); 
    [NE,Ne] = eig(dist_cov);
    if (qr.ENABLE_DISTURBANCES)
        disturb = NE*sqrt(Ne)*randn(6,1); 
    else
        disturb = zeros(6,1);
    end

    % TBD wind effects on drag forces and moments, thrust, torque
    
    inertial_forces = f_t - f_d - f_g + disturb(1:3);
    
    % Rigid body motion
    qr.pos = qr.pos + qr.dt*qr.vel; % Inertial position
    qr.vel = qr.vel + qr.dt .* inertial_forces ./ qr.mass;
    qr.rpy = qr.rpy + (qr.dt*R_e*qr.pqr); % Euler angles
    qr.rpy = mod(qr.rpy+pi,2*pi)-pi;
    qr.pqr = qr.pqr + qr.dt*(qr.I \ (M_t - M_d + disturb(4:6) ...
        - cross(qr.pqr, qr.I*qr.pqr))); % Body angular velocities
    qr.rot_omega = qr.rot_omega + 1/qr.Jr * qr.dt*(-tau - ...
        (qr.k_q*qr.k_e/qr.Ra)*qr.rot_omega + ...
        (qr.k_q/qr.Ra)*qr.mot_v); % Rotor angular velocities
    
    %% Update sensors
    qr.gps_ticks = qr.gps_ticks + 1;
    if qr.gps_ticks >= qr.GPS_TICK_RATE
        qr = updategps(qr);
        qr.gps_ticks = 0;
    end

    qr.barom_ticks = qr.barom_ticks + 1;
    if qr.barom_ticks >= qr.BAROM_TICK_RATE
        qr = updatebarom(qr);
        qr.barom_ticks = 0;
    end

    qr.sonar_ticks = qr.sonar_ticks + 1;
    if qr.sonar_ticks >= qr.SONAR_TICK_RATE
        qr = updatesonar(qr);
        qr.sonar_ticks = 0;
    end
    
    qr.imu_ticks = qr.imu_ticks + 1;
    if qr.imu_ticks >= qr.IMU_TICK_RATE
        accel_meas = R'*(inertial_forces./ qr.mass - [0; 0; 9.81]);
        

        
%         z_temp = R'*f_t - R'*f_g;
%         body_forces = [f_dx; f_dy; (z_temp(3) - f_dz)];
        qr = updateimu(qr, accel_meas);
        qr.imu_ticks = 0;
    end
end

%% Controller subroutines
function [ qr ] = updateposctrl( qr )
    err = qr.des_pos - qr.pos;
%     err(3) = max(-1, min(1, err(3)));
    err_d = qr.des_vel - qr.vel;
%     err(3) = max(-2, min(2, err(3)));
    qr.err_pos_int = qr.err_pos_int + err.*qr.POS_CTRL_DT;
    
    ux = qr.Kxy*[err(1), err_d(1), qr.err_pos_int(1)]'; 
    uy = qr.Kxy*[err(2), err_d(2), qr.err_pos_int(2)]'; 
    uz = qr.Kz*[err(3), err_d(3), qr.err_pos_int(3), 1]';
    
    qr.ctrl_sigs(1:3) = [ux, uy, uz];
    temp_des_rpy = rot(qr.rpy(3), 3) * [ux, uy, 0]';
    temp_des_rpy(1) = min( qr.MAX_RP, ...
        max(temp_des_rpy(1), -qr.MAX_RP) );
    temp_des_rpy(2) = min( qr.MAX_RP, ...
        max(temp_des_rpy(2), -qr.MAX_RP) );
    temp_des_rpy = [-temp_des_rpy(2); temp_des_rpy(1)];
    qr.des_rpy(1:2) = temp_des_rpy;
    
    qr.des_rpy_rate(1:2) = (temp_des_rpy - qr.des_rpy(1:2)) / ...
        qr.POS_CTRL_DT;
    qr.des_rpy_rate(1) = min( qr.MAX_RP_RATE, ...
        max(qr.des_rpy_rate(1), -qr.MAX_RP_RATE) );
    qr.des_rpy_rate(2) = min( qr.MAX_RP_RATE, ...
        max(qr.des_rpy_rate(2), -qr.MAX_RP_RATE) );
end

function [ qr ] = updateattctrl( qr )
    err = qr.des_rpy - qr.rpy;
    err = mod(err + pi, 2*pi) - pi;
    err_d = qr.des_rpy_rate - re(qr.rpy, 1)*qr.pqr;
    err_dd = [0;0;0]; % Currently unused
    qr.err_att_int = qr.err_att_int + err.*qr.ATT_CTRL_DT;
    
    uphi = qr.Katt*[err(1), err_d(1), err_dd(1), qr.err_att_int(1)]';
    utheta = qr.Katt*[err(2), err_d(2), err_dd(2), qr.err_att_int(2)]';
    upsi = qr.Katt*[err(3), err_d(3), err_dd(3), qr.err_att_int(3)]';
    qr.ctrl_sigs(4:6) = [uphi, utheta, upsi];
    
    u = qr.Mrc*[qr.ctrl_sigs(4:6); qr.ctrl_sigs(3)]; % Desired thrusts
    u = min(qr.Td_max, max(0, u)); % bounded desired thrusts
    des_omega = sqrt(u / qr.k_t);
    qr.des_rot_omega = des_omega;
end

function [ qr ] = updatemotorctrl( qr )
    err = qr.des_rot_omega - qr.rot_omega;
    qr.err_mot_int = qr.err_mot_int + qr.dt.*err;
    
    for jj=1:4
        Vi_ff = qr.Ra/qr.k_q*qr.k_tau*qr.k_t*qr.des_rot_omega(jj)^2 ...
            + qr.k_e*qr.des_rot_omega(jj);
        Vi_fb = qr.Kms*[err(jj); qr.err_mot_int(jj)];
        Vi = Vi_ff +Vi_fb;
        Vi = min(qr.V_max,max(Vi,0)); % Bound voltage command
        
        qr.mot_v(jj) = Vi;
    end
end

%% Sensor subroutines

function [ qr ] = updategps( qr )
    qr.gps = qr.pos + qr.gps_bias + randn(3,1).*qr.GPS_COV;
    qr.gps_bias = qr.gps_bias + -(1/qr.GPS_TAU).*qr.gps_bias + ...
        randn(3,1).*qr.GPS_BIAS_COV;
    
    qr.gps_vel = qr.vel + randn(3,1).*qr.GPS_VEL_COV;
end

function [ qr ] = updatebarom( qr )
    qr.barom = qr.pos(3) + qr.barom_bias + randn(1)*qr.BAROM_COV;
    qr.barom_bias = qr.barom_bias -(1/qr.BAROM_TAU).*qr.barom_bias + ...
        randn(1)*qr.BAROM_BIAS_COV;

%     qr.barom_bias = qr.barom_bias + randn(1)*qr.BAROM_BIAS_COV;

end

function [ qr ] = updatesonar( qr )
    % SONAR_MAX_HEIGHT = 4.25m
    if (qr.pos(3) > 4.25 || rand(1) > 0.8) && qr.ENABLE_SONAR_NOISE
        qr.sonar = 3.9 + rand(1)*0.6; % random value from 3.9..4.5m
    else
        qr.sonar = ( 1 / (cos(qr.rpy(1))*cos(qr.rpy(2))) )*qr.pos(3) +...
            randn(1)*qr.SONAR_COV;
    end
end

function [ qr ] = updateimu( qr, accel_meas )
    % 9-axis IMU: 3 gyrometers, 3 accelerometers, and 3 magnetometers
    % One of each type of sensor is assumed to be perfectly aligned with
    % each body axis.
    
    
    qr.gyro = qr.pqr + qr.gyro_bias + randn(3,1).*qr.GYRO_COV;
%     qr.gyro_bias = qr.gyro_bias - (1/(qr.GYRO_TAU/dt)).*qr.gyro_bias + ...
%         randn(3,1).*qr.GYRO_BIAS_COV;
    qr.gyro_bias = qr.gyro_bias + randn(3,1).*qr.GYRO_BIAS_COV;

    qr.mag = qr.rpy(3) + qr.mag_bias + randn(1)*qr.MAG_COV;
    qr.mag_bias = qr.mag_bias + randn(1)*qr.MAG_BIAS_COV - ...
        (1/qr.MAG_TAU).*qr.mag_bias;
    
%     qr.accel_clean = body_forces./qr.mass;
    qr.accel_clean = accel_meas;
    
    qr.accel = qr.accel_clean + randn(3,1).*qr.ACCEL_COV;
end

function wind = generate_wind(qr)
    if (~qr.ENABLE_WIND)
        wind = zeros(3,1);
        return
    end

    persistent time
    if isempty(time)
        time = 0;
    end
    
    wind_start = [-8;10;0.3];
    for i = 1:20
        wind_component_x = sin(0.008*i*1.12*time - pi/7 + pi/9*i);
        wind_component_y = sin(0.011*0.9*i*time - pi/3 + pi/7*i);
        wind_component_z = sin(0.010*i*time + pi/11 + pi/5*i);

        wind(1) = wind_start(1) + wind_component_x;
        wind(2) = wind_start(2) + wind_component_y;
        wind(3) = wind_start(3) + wind_component_z;
    end
    wind = wind';
    time = time + qr.dt;
end



