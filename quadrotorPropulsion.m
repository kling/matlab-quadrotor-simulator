function [y,xpred] = quadrotorPropulsion(xin, uin, parameter, dt)


% Constants
CT0s = parameter.CT0s;
CT1s = parameter.CT1s;
CT2s = parameter.CT2s;
k_t  = parameter.k_t;
l_m  = parameter.l_m;

v_1(1) = - w + l_m*q;
v_1(2) = - w - l_m*p;
v_1(3) = - w - l_m*q;
v_1(4) = - w + l_m*p;

% calculate thrust for all 4 rotors
for i = 1:4
    % if the flow speed at infinity is negative
    if v_1(i) < 0
        F_m(i) = CT2s*v_1(i).^2 + CT1s*v_1(i).*xin(i) + CT0s*xin(i).^2;
        % if the flow speed at infinity is positive
    else
        F_m(i) = -CT2s*v_1(i).^2 + CT1s*v_1(i).*xin(i) + CT0s*xin(i).^2;
    end
    % sum up all rotor forces
    F(3) = F(3) + F_m(i) ;
    
    [M_e(i),xpred(i)] = motorspeed(xin(i), [U(i) k_t*F_m(i)], parameter, dt);
end

% System output, i.e. force and torque of quadrotor
y(1) = F(1);
y(2) = F(2);
y(3) = F(3);

% torque for rotating quadrocopter around x-axis is the mechanical torque
y(4) = (F_m(4)-F_m(2))*l_m;
% torque for rotating quadrocopter around y-axis is the mechanical torque
y(5) = (F_m(1)-F_m(3))*l_m;
% torque for rotating quadrocopter around z-axis is the electrical torque
y(6) = (-M_e(1)-M_e(3)+M_e(2)+M_e(4));