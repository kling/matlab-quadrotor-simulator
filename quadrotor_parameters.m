%% Quadrotor model parameters
N = 16; % number of state variables
M = 4; % number of inputs

% Call motor and environment parameters
motor_parameters;

% Vehicle
m_c = 0.400; % kg mass of quadrotor core
m_m = 0.15; % kg mass of quadrotor motors
m = m_c + 4*m_m; % mass of quadrotor 
l_c = 0.1; % m center length
l_m = 0.8; % m  arm length

% Inertia calculated from four motors and a cube center.
Ix = 1/12*m_c*l_c^2 + 2*(1/2*m_m*l_m^2); 
Iy = Ix;
Iz = 1/12*m_c*(l_c^2+l_c^2) + 4*(1/2*m_m*l_m^2);
I = diag([Ix, Iy, Iz]);
% Frontal area assumes l_c cube at center, 1 cm arms and 4cm X1cm radius
% motor cylinders.  Ignores rotors, as their drag is accounted for
% elsewhere
Axy = l_c^2 + 2*0.01*0.70 + 2*0.04*0.02; % x-y frontal area
Az = l_c^2 + 4*pi*0.01^2 + 4*0.01*0.7; % z frontal area
c_d = 1.0; % translational coefficient of drag
c_m = 1.0; % rotational coefficient of drag

Mcr = [   0  l_m     0   -l_m;
        -l_m     0  l_m     0; % Conversion from rotor pwm to control torques/thrust
        -c_m  c_m   -c_m  c_m;
        1    1    1    1];

Mrc = [0  -1/(2*l_m)   -1/(4*c_m) 1/4; % Conversion from control torques/thrust to rotor pwm
       1/(2*l_m) 0 1/(4*c_m) 1/4;
       0  1/(2*l_m)  -1/(4*c_m) 1/4;
        -1/(2*l_m) 0 1/(4*c_m) 1/4];
%Mrc = inv(Mcr); % equivalent

%  Drag coefficients
Cdxy   = 1/2*Axy*rho*c_d; 
Cdz    = 1/2*Az*rho*c_d;  
Cmxy   = 1/2*Az*rho*c_m;  
Cmz    = 1/2*Axy*rho*c_m; 

% Ground effect coefficients
Kge = 0.5; % Ground effect percent thrust increase
hge = 1.0; % Maximum height for ground effect model to have effect


%% Quadrotor controller parameters
% Position control
Kpxy = 2;
Kdxy = 1;
Kixy = 0.0;
Kxy = [Kpxy,Kdxy,Kixy];

% Altitude control
Kpz = 10;
Kdz = 5;
Kiz = 0.0;
Kgz = m*g; % gravity offset, base desired force to overcome gravity
Kz = [Kpz,Kdz,Kiz,Kgz];

% Roll/Pitch control
Kpatt = 10;
Kdatt = 2;
Kddatt = 0.0;
Kiatt = .1;
Katt = [Kpatt,Kdatt,Kddatt,Kiatt];
PhiThetaMax = 30*pi/180;

% Yaw control
Kpyaw = 5;
Kdyaw = 2;
Kddyaw = 0;
Kiyaw = 2;
Kyaw = [Kpyaw,Kdyaw,Kddyaw,Kiyaw];

% Motor speed control
Kpms = 1;
Kims = 0.0;
Kms = [Kpms,Kims];
Td_max = 15; % (N) Max thrust request per motor