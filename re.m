function Re_out=Re(angles, b2e)
%Function R=Re(angles (phi,theta,psi (rad), b2e) returns a 3x3
%conversion matrix for rotating a vector of pqr rates to euler angle rates 
%or vice versa.  If b2e = 1, the pqr vector is passed in and converted to
%euler rates.  If b2e = 0, the euler rate vector is passed in and converted
%to body rates pqr.
c1 = cos(angles(1));
c2 = cos(angles(2));
c3 = cos(angles(3));
s1 = sin(angles(1));
s2 = sin(angles(2));
s3 = sin(angles(3));

if (b2e)
    Re_out = [1 s1*s2/c2 c1*s2/c2;
          0 c1 -s1;
          0 s1/c2 c1/c2];
else
    Re_out = [1 0 -s2;
          0 c1 c2*s1;
          0 -s1 c1*c2];
end
return;